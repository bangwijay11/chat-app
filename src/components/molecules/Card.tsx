import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {responsiveSize} from '../../utils/ResponsiveSize';
import {Button} from '../atoms';

interface ICard {
  title: string;
  edition_count: number | string;
  authors: any;
  onPress?: () => void;
  testID?: string;
}

const Card = ({title, edition_count, authors, onPress, testID}: ICard) => {
  return (
    <View style={styles.item}>
      <View style={styles.img} />
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.title}>Edition number: {edition_count}</Text>
      <View style={{marginTop: 8}}>
        {authors?.map((author: any) => (
          <Text key={author.key} style={styles.text}>
            - {author.name}
          </Text>
        ))}
      </View>
      <Button testID={testID || 'loanBook'} title="Loan" onPress={onPress} />
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    width: responsiveSize(165),
    minHeight: responsiveSize(250),
    backgroundColor: 'white',
    padding: responsiveSize(16),
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#EBF0FF',
  },
  img: {
    width: responsiveSize(133),
    height: responsiveSize(133),
    backgroundColor: 'grey',
    borderRadius: 10,
    marginBottom: responsiveSize(8),
  },
  title: {
    color: '#223263',
    fontSize: responsiveSize(12),
    fontWeight: '700',
  },
  text: {
    color: '#223263',
    fontSize: responsiveSize(8),
    fontWeight: '400',
  },
});

export default Card;
