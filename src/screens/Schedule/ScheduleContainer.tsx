import React, {useState} from 'react';
import ScheduleComponent from './ScheduleComponent';
import {Alert} from 'react-native';

const ScheduleContainer = (props: {route: any; navigation: any}) => {
  const [date, setDate] = useState(new Date());
  const [time, setTime] = useState(new Date());
  const [openDate, setOpenDate] = useState(false);
  const [openTime, setOpenTime] = useState(false);

  const {
    route: {
      params: {bookData},
    },
    navigation,
  } = props;

  const handleSelectDate = (value: any) => {
    setDate(value);
    setOpenDate(false);
  };
  const handleSelectTime = (value: any) => {
    setTime(value);
    setOpenTime(false);
  };
  const handleOpenModalDate = (open: boolean) => setOpenDate(open);
  const handleOpenModalTime = (open: boolean) => setOpenTime(open);

  const onSubmit = () => {
    // const newLoaned = {
    //   bookInformation: bookData,
    //   date,
    //   time,
    //   key: bookData.key,
    // };
    // setBookLoaned([...bookLoaned, newLoaned]);

    Alert.alert('Success', 'Your book borrowing request has been sent', [
      {text: 'OK', onPress: () => navigation.goBack()},
    ]);
  };

  return (
    <ScheduleComponent
      bookData={bookData}
      date={date}
      time={time}
      openDate={openDate}
      openTime={openTime}
      handleSelectDate={handleSelectDate}
      handleSelectTime={handleSelectTime}
      handleOpenModalDate={handleOpenModalDate}
      handleOpenModalTime={handleOpenModalTime}
      onSubmit={onSubmit}
    />
  );
};

export default ScheduleContainer;
