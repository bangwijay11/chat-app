import {StyleSheet, View} from 'react-native';
import React from 'react';
import {responsiveSize} from '../../utils/ResponsiveSize';
import {OptionButton} from '../atoms';

interface IGenreOptions {
  Options: any[];
  activeGenre: string;
  onPress: (option: string) => void;
}

const GenreOptions = ({Options, activeGenre, onPress}: IGenreOptions) => {
  return (
    <View style={styles.optionGenre}>
      {Options.map((option: any, index: any) => {
        const isActive = option === activeGenre;
        return (
          <OptionButton
            testID={'genreOption'}
            isActive={isActive}
            option={option}
            key={index}
            onPress={() => onPress(option)}
          />
        );
      })}
    </View>
  );
};
const styles = StyleSheet.create({
  optionGenre: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: responsiveSize(16),
    gap: 16,
  },
});

export default GenreOptions;
